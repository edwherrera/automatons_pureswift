import Foundation

extension String {
    func Substring(from:Int) -> String{
        return self.substringFromIndex(self.startIndex.advancedBy(from))
    }
    
    subscript (i:Int) -> Character{
        return self[self.startIndex.advancedBy(i)]
    }
}