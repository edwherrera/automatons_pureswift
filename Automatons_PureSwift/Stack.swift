//
//  Stack.swift
//  Automatons_PureSwift
//
//  Created by Edwin Herrera on 13/02/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

import Foundation

class Stack {
    private var elementArray = [AnyObject]()
    
    func Push(object:AnyObject){
        self.elementArray.append(object)
    }
    
    func Pop() -> AnyObject?{
        return elementArray.removeLast()
    }
    
    func Count() -> Int{
        return self.elementArray.count
    }
}