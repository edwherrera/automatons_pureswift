import Foundation

let EPSILON = "ε"

class NondeterministicFiniteAutomaton: DeterministicFiniteAutomaton {
    
    private func GetAllTransitionNames() -> [String]{
        var transitionNames = [String]()
        for state in self.States{
            for transition in state.Transitions{
                if !transitionNames.contains(transition.Name) {
                    transitionNames.append(transition.Name)
                }
            }
        }
        return transitionNames
    }
    
    private func GetExits(currentState:State, transitionName:String) -> [State] {
        var exitStates = [State]()
        
        for transition in currentState.Transitions {
            if transition.Name == transitionName {
                exitStates.append(transition.DestinyState)
            }
        }
        
        return exitStates
    }
    
    private func Union(states:[State], transitionName:String) -> [State]{
        var stateUnion = [State]()
        
        for state in self.States{
            let exitStates = self.GetExits(state, transitionName: transitionName)
            for exitState in exitStates {
                if !stateUnion.contains({ (element) -> Bool in
                    element === exitState
                }){
                    stateUnion.append(exitState)
                }
            }
        }
        
        return stateUnion
    }
    
    private func EvaluateString(stringToEvaluate: String, state:State) -> Bool {
        if stringToEvaluate.characters.count == 0 { return state.IsFinal }
        
        for transition in state.Transitions {
            if transition.Name == String(stringToEvaluate[0]) {
                if self.EvaluateString(stringToEvaluate.Substring(1), state: transition.DestinyState){
                    
                }
            }
        }
        
        return false
    }
    
    override func EvaluateString(stringToEvaluate: String) -> Bool {
        if self.InitialState == nil { return false }
        return self.EvaluateString(stringToEvaluate, state: self.InitialState!)
    }
    
    private func StateArraysContainSameElements(array1:[State], array2:[State]) -> Bool {
        return array1.elementsEqual(array2, isEquivalent: { (state1, state2) -> Bool in
            state1.Name == state2.Name
        })
    }
    
    private func PossibleRoadsArrayContainsUnion(possibleRoads:[[State]], union:[State]) -> Bool {
        for road in possibleRoads {
            if self.StateArraysContainSameElements(road, array2: union) { return true }
        }
        return false
    }
    
    override func SetTransition(fromStateName: String, toStateName: String, transitionName: String) -> Transition? {
        guard let fromState = self.GetState(fromStateName), toState = self.GetState(toStateName) else {
            return nil
        }
        
        guard let _ = fromState.GetTransition(transitionName, destinyStateName: toState.Name) else{
            return fromState.AddTransition(transitionName, destinyState: toState)
        }
        
        return nil
    }
    
    private func GetEpsilonClosures(inout states:[State]){
        var process = [State]()
        process.appendContentsOf(states)
        
        while !process.isEmpty {
            let fromState = process.removeFirst()
            for transition in fromState.GetTransitions(EPSILON) {
                if(!states.contains({ (element) -> Bool in
                    element.Name == transition.DestinyState.Name
                })){
                    states.append(transition.DestinyState)
                    process.append(transition.DestinyState)
                }
            }
        }
    }
    
    private func EvaluateEpsilonString(stringToEvaluate:String, state:State) -> Bool{
        var fromStates = [State]()
        fromStates.append(state)
        self.GetEpsilonClosures(&fromStates)
        
        for c in stringToEvaluate.characters {
            if fromStates.isEmpty { return false }
            var toStates = [State]()
            for fromState in fromStates {
                for transition in fromState.GetTransitions(String(c)){
                    if !toStates.contains({ (element) -> Bool in
                        element.Name == transition.DestinyState.Name
                    }){
                        toStates.append(transition.DestinyState)
                    }
                }
            }
            self.GetEpsilonClosures(&toStates)
            fromStates = toStates
        }
        
        for stateCheck in fromStates {
            if stateCheck.IsFinal { return true }
        }
        
        return false
    }
    
    func EvaluateEpsilonString(stringToEvaluate:String) -> Bool {
        if self.InitialState == nil { return false }
        return self.EvaluateEpsilonString(stringToEvaluate, state: self.InitialState!)
    }
    
    func EliminateEpsilonExits() {
        struct ToNonEpsilonNFAStorageHelper {
            var originState:State
            var transitionName:String
            var destinyStates:[State]
        }
        
        var result = [ToNonEpsilonNFAStorageHelper]()
        let transitionNames = {() -> [String] in
            let mut = NSMutableArray(array: self.GetAllTransitionNames() as NSArray)
            mut.removeObject(EPSILON)
            return (mut as NSArray) as! Array
            }()
        let newNFA = NondeterministicFiniteAutomaton()
        
        for state in self.States {
            for transitionName in transitionNames {
                var closures = [State]()
                var firstMoveTransitions = state.GetTransitions(EPSILON)
                firstMoveTransitions.append(Transition(name: EPSILON, destinyState: state))
                for firstMoveTransition in firstMoveTransitions {
                    let secondMoveTransitions = firstMoveTransition.DestinyState.GetTransitions(transitionName)
                    for secondMoveTransition in secondMoveTransitions {
                        var thirdMoveTransitions = secondMoveTransition.DestinyState.GetTransitions(EPSILON)
                        thirdMoveTransitions.append(Transition(name: EPSILON, destinyState: secondMoveTransition.DestinyState))
                        for toAdd in thirdMoveTransitions {
                            if !closures.contains({ (element) -> Bool in
                                toAdd.DestinyState.Name == element.Name
                            }){
                                closures.append(toAdd.DestinyState)
                            }
                        }
                    }
                }
                result.append(ToNonEpsilonNFAStorageHelper(originState: state, transitionName: transitionName, destinyStates: closures))
            }
            newNFA.AddState(state.Name, isFinal: state.IsFinal)
        }
        if self.InitialState != nil{
            newNFA.SetInitialState(self.InitialState!.Name)
        }
        
        for helper in result {
            for state in helper.destinyStates {
                newNFA.SetTransition(helper.originState.Name, toStateName: state.Name, transitionName: helper.transitionName)
            }
        }
        
        self.States = newNFA.States
        self.InitialState = newNFA.InitialState
    }
    
    override func MinimizedCopy() throws -> DeterministicFiniteAutomaton {
        throw NSError(domain: "Doesn't work with NFA", code: -1, userInfo: nil)
    }
    
}