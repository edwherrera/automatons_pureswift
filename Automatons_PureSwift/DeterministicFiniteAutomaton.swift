import Foundation

class DeterministicFiniteAutomaton {
    var States:[State]
    var InitialState:State?
    
    init(){
        States = [State]()
        InitialState = nil
    }
    
    private func GetAllTransitionNames() -> [String]{
        var transitionNames = [String]()
        for state in self.States{
            for transition in state.Transitions{
                if !transitionNames.contains(transition.Name) {
                    transitionNames.append(transition.Name)
                }
            }
        }
        return transitionNames
    }
    
    func GetState(stateName:String) -> State?{
        for state in States{
            if(state.Name == stateName){ return state }
        }
        return nil
    }
    
    func AddState(stateName:String, isFinal:Bool) -> State?{
        if (GetState(stateName) != nil) { return nil }
        let newState = State(name: stateName, isFinal: isFinal)
        States.append(newState)
        if self.InitialState == nil {
            self.InitialState = newState
        }
        return newState
    }
    
    func AddState(stateName:String) -> State?{
        return self.AddState(stateName, isFinal: false)
    }
    
    func AddStates(stateNames:String...){
        for stateName in stateNames{
            self.AddState(stateName)
        }
    }
    
    func SetInitialState(stateName:String) -> Bool{
        guard let foundState = self.GetState(stateName) else{
            return false
        }
        self.InitialState = foundState
        return true
    }
    
    func SwitchStateToOrFromFinal(stateName:String) -> Bool {
        guard let foundState = self.GetState(stateName) else {
            return false
        }
        
        foundState.IsFinal = !foundState.IsFinal
        return true
    }
    
    func SetTransition(fromStateName:String, toStateName:String, transitionName:String) -> Transition?{
        guard let fromState = self.GetState(fromStateName), toState = self.GetState(toStateName) else{
            return nil
        }
        
        let foundTransitions = fromState.GetTransitions(transitionName)
        
        if foundTransitions.count > 0 {
            foundTransitions[0].DestinyState = toState
            return foundTransitions[0]
        }
        
        return fromState.AddTransition(transitionName, destinyState: toState)
    }
    
    func DeleteTransition(fromStateName:String, toStateName:String, transitionName:String) -> Bool{
        guard let fromState = self.GetState(fromStateName), toState = self.GetState(toStateName) else{
            return false
        }
        return fromState.DeleteTransition(transitionName, destinyState: toState)
    }
    
    func RemoveUnusedTransitionsAfterDeletingState(deletedState:State){
        for state in self.States{
            state.DeleteTransitions(deletedState.Name)
        }
    }
    
    func DeleteState(stateName:String) -> Bool{
        for (index, state) in self.States.enumerate(){
            if state.Name == stateName {
                self.RemoveUnusedTransitionsAfterDeletingState(state)
                self.States.removeAtIndex(index)
                if self.States.count == 0 {
                    self.InitialState = nil
                }
                else if self.InitialState == nil {
                    self.InitialState = self.States.first
                }
                return true
            }
        }
        
        return false
    }
    
    func EvaluateString(stringToEvaluate:String) -> Bool{
        guard var state = self.InitialState else {
            return false
        }
        
        for c in stringToEvaluate.characters{
            guard let transition = state.GetTransitions(String(c)).first else{
                return false
            }
            state = transition.DestinyState
        }
        
        return state.IsFinal
    }
    
    func MinimizedCopy() throws -> DeterministicFiniteAutomaton {
        
        func ExitsAreInEquivalentGroups(state1:State?, state2:State?, equivalentGroups:[[State]]) -> Bool {
            if state1?.Name == state2?.Name { return true }
            for equivalentGroup in equivalentGroups {
                var found = false
                for state in equivalentGroup {
                    if state.Name == state1?.Name || state.Name == state2?.Name{
                        if found { return true }
                        else { found = true }
                    }
                }
                if found { return false }
            }
            
            return true
        }
        
        var finalStates = [State]()
        var nonFinalStates = [State]()
        
        States.forEach { (state) -> () in
            if state.IsFinal { finalStates.append(state) }
            else { nonFinalStates.append(state) }
        }
        
        var equiStates = [[State]]()
        if nonFinalStates.count > 0 { equiStates.append(nonFinalStates) }
        if finalStates.count > 0 { equiStates.append(finalStates) }
        let transitionNames = GetAllTransitionNames()
        
        while true {
            var tmpStates = [[State]]()
            var newGroup = [State]()
            
            for equiState in equiStates {
                tmpStates.append([State](arrayLiteral: equiState.first!))
                for state in equiState {
                    if state.Name == equiState.first!.Name { continue }
                    var exitsAreEquivalent = true
                    for transitionName in transitionNames {
                        if !ExitsAreInEquivalentGroups(state.GetTransitions(transitionName).first?.DestinyState, state2: equiState.first?.GetTransitions(transitionName).first?.DestinyState, equivalentGroups: equiStates){
                            exitsAreEquivalent = false
                        }
                    }
                    if exitsAreEquivalent {
                        tmpStates[tmpStates.count-1].append(state)
                    }
                    else {
                        newGroup.append(state)
                    }
                }
            }
            equiStates = tmpStates
            if newGroup.count == 0 {
                break
            }
            equiStates.append(newGroup)
        }
        
        let minimizedDFA = DeterministicFiniteAutomaton()
        var minimizedStates = [State]()
        
        var initialState = ""
        for equiState in equiStates {
            var newName = ""
            var isFinal = false
            var isInitial = false
            for state in equiState {
                newName += state.Name
                if state.IsFinal { isFinal = true }
                if state.Name == InitialState?.Name { isInitial = true }
            }
            minimizedStates.append(State(name: newName, isFinal: isFinal))
            if isInitial { initialState = newName }
        }
        
        for (i, equiState) in equiStates.enumerate() {
            for state in States {
                if state.Name == equiState.first?.Name {
                    for transition in state.Transitions {
                        for (j, destinies) in equiStates.enumerate() {
                            if destinies.contains({ (destiny) -> Bool in
                                destiny.Name == transition.DestinyState.Name
                            }){
                                minimizedStates[i].Transitions.append(Transition(name: transition.Name, destinyState: minimizedStates[j]))
                                break
                            }
                        }
                    }
                    break
                }
            }
        }
        
        minimizedDFA.States = minimizedStates
        minimizedDFA.SetInitialState(initialState)
        
        return minimizedDFA
    }
    
    static func Merge(automaton1:DeterministicFiniteAutomaton, automaton2:DeterministicFiniteAutomaton) -> DeterministicFiniteAutomaton {
        
        
        
        return DeterministicFiniteAutomaton()
    }
}