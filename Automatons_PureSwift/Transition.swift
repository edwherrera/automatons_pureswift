import Foundation

class Transition {
    var Name:String
    var DestinyState:State
    init(name:String, destinyState:State){
        Name = name
        DestinyState = destinyState
    }
}