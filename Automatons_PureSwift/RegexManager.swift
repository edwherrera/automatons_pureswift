//
//  RegexManager.swift
//  Automatons_PureSwift
//
//  Created by Edwin Herrera on 01/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

import Foundation

func automatonWithRegex(regex:String) -> NondeterministicFiniteAutomaton {
    let nfa = NondeterministicFiniteAutomaton()
    let ops = Stack()
    
    for i in 0..<regex.characters.count {
        nfa.AddState("q\(i)")
    }
    nfa.AddState("q\(regex.characters.count)", isFinal: true)
    
    for i in 0..<regex.characters.count {
        var lp = i
        if regex[i] == "(" || regex[i] == "|" {
            ops.Push(i)
        }
        else if regex[i] == ")"{
            let or = ops.Pop() as! Int
            if regex[or] == "|" {
                lp = ops.Pop() as! Int
                nfa.SetTransition(nfa.States[lp].Name, toStateName: nfa.States[or+1].Name, transitionName: EPSILON)
                nfa.SetTransition(nfa.States[or].Name, toStateName: nfa.States[i].Name, transitionName: EPSILON)
            }
            else {
                lp = or
            }
        }
        
        else if regex[i] != "(" && regex[i] != ")" && regex[i] != "*" && regex[i] != "|" {
            nfa.SetTransition(nfa.States[i].Name, toStateName: nfa.States[i+1].Name, transitionName: String(regex[i]))
        }
        
        if i < regex.characters.count-1 && regex[i+1] == "*" {
            nfa.SetTransition(nfa.States[lp].Name, toStateName: nfa.States[i+1].Name, transitionName: EPSILON)
            nfa.SetTransition(nfa.States[i+1].Name, toStateName: nfa.States[lp].Name, transitionName: EPSILON)
        }
        
        if regex[i] == "(" || regex[i] == "*" || regex[i] == ")" {
            nfa.SetTransition(nfa.States[i].Name, toStateName: nfa.States[i+1].Name, transitionName: EPSILON)
        }
        
    }
    
    return nfa
}