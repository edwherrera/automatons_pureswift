import Foundation

class State{
    var Name:String
    var Transitions:[Transition]
    var IsFinal:Bool
    
    init(name:String, isFinal:Bool){
        Name = name
        IsFinal = isFinal
        Transitions = [Transition]()
    }
    
    convenience init(name:String){
        self.init(name: name, isFinal: false)
    }
    
    func GetTransition(transitionName:String, destinyStateName:String) -> Transition?{
        
        for transition in Transitions{
            if transition.Name == transitionName && transition.DestinyState.Name == destinyStateName{
                return transition
            }
        }
        return nil
    }
    
    func GetTransitions(transitionName:String) -> [Transition]{
        var transitionsMatch = [Transition]()
        
        for transition in self.Transitions{
            if transition.Name == transitionName {
                transitionsMatch.append(transition)
            }
        }
        
        return transitionsMatch
    }
    
    func AddTransition(transitionName:String, destinyState:State) -> Transition?{
        let newTransition:Transition = Transition(name: transitionName, destinyState: destinyState)
        self.Transitions.append(newTransition)
        return newTransition
    }
    
    func DeleteTransition(transitionName:String, destinyState:State) -> Bool{
        for (index, transition) in Transitions.enumerate(){
            if(transition.Name == transitionName && transition.DestinyState.Name == destinyState.Name){
                Transitions.removeAtIndex(index)
                return true
            }
        }
        return false
    }
    
    func DeleteTransitions(destinyStateName:String){
        for var i = 0; i < self.Transitions.count; i++ {
            if self.Transitions[i].DestinyState.Name == destinyStateName{
                Transitions.removeAtIndex(i)
                i--
            }
        }
    }
}