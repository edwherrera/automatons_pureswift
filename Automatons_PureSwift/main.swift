import Foundation

let nfa = automatonWithRegex("((A*B|AC)*D*)")
nfa.EliminateEpsilonExits()
print(nfa.EvaluateEpsilonString("DDD"))
print("")
